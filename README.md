PlanThouLife

Introduction:
This scheduling tool will assist in day to day life by allowing users a simple interface for scheduling their life. It uses simple MEAN.js framework and is cross compatible across devices.

Requirements:
	Up to date device.

How to Access:
Navigate to http://dana.ucc.nau.edu/vjm36/hw-app/#!/home to access the application. From here you will see Plan Thou Life, which is our prototype for PlanMyLife